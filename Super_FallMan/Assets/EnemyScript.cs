﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public GameObject self;
    public float speed;
    private float movetime = 1;
    private bool facingRight = true;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet")) {

            Destroy(self);
            Debug.Log("Hit Enemy");
        
        }
        Debug.Log("Hit Enemy");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (movetime <= 0)
            flip();
        if (facingRight)
            transform.position = new Vector3(transform.position.x+speed,transform.position.y,transform.position.z);
        else
            transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
        movetime -= Time.deltaTime;

    }

    private void flip() {

        facingRight = !facingRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
        movetime = 1;

    }
}
