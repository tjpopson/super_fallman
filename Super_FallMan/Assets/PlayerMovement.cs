﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    public Rigidbody2D rb;
    public GameObject player;
    public float playerSpeed;
    public int maxAmmo = 5;
    public int maxHealth = 4;
    public Text healthText;
    public Text ammoText;
    public GameObject shot;
    public Animator anim;

    private bool facingRight;
    private int ammo;
    private float moveInput;
    private int health;

    private bool onGround = false;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    public Transform hazardCheck;
    private Vector2 hazardCheckSize;
    public LayerMask whatIsHazard;
    private float invincibilityTime = 1f;
    private bool hit = false;

    private void Start()
    {
        facingRight = true;
        ammo = maxAmmo;
        health = maxHealth;
        healthText.text = "Health: " + health;
        ammoText.text = "Ammo: " + ammo;
        hazardCheckSize = transform.localScale;
    }

    private void Update()
    {
        if (onGround)
        {
            ammo = maxAmmo;
        }
        healthText.text = "Health: " + health;
        ammoText.text = "Ammo: " + ammo;
        anim.SetFloat("player_speed", Mathf.Abs(rb.velocity.x));
        anim.SetFloat("vertical_speed", rb.velocity.y);
        anim.SetBool("onGround",onGround);

        if (health <= 0) {

            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);

        }

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, transform.localScale);
    }

    private void FixedUpdate()
    {
        //get player movement input
        moveInput = Input.GetAxisRaw("Horizontal");

        //check if player is on ground
        onGround = Physics2D.OverlapCircle(groundCheck.position,checkRadius,whatIsGround);

        if (hit == true)
            invincibilityTime -= Time.deltaTime;
        if (invincibilityTime <= 0)
        {
            hit = false;
            invincibilityTime = 1f;
        }

        if (Physics2D.OverlapBox(transform.position,hazardCheckSize,0f,whatIsHazard) && hit == false) {
            Hit();
        }
        

        //move the player
        move();

        //flip the sprite on direction change
        if ((facingRight == true && moveInput < 0) || (facingRight == false && moveInput > 0)) {
            flip();
        }

        //check if the player is pressing space
        if (Input.GetKeyDown(KeyCode.Space))
            jump();

    }

    //flips the character sprite
    private void flip() {

        facingRight = !facingRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    
    }

    //checks if the player is on the ground and if so then jumps else shoots
    private void jump() {

        if (onGround)
        {

            rb.AddForce(new Vector2(0, 10000f));

        }
        else {

            shoot();
        
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy")) {

            Hit();
            Destroy(collision.gameObject);
        
        }
    }

    private void move()
    {
        rb.velocity = new Vector2(moveInput*playerSpeed, rb.velocity.y);
    }

    private void shoot() {
        if (ammo > 0)
        {
            ammo--;
            rb.AddForce(new Vector2(0, 5000f));
            Instantiate(shot,new Vector3(transform.position.x,transform.position.y-(transform.localScale.y/2),1),transform.rotation);
        }
    }

    private void Hit()
    {
        if (health > 0) {
            hit = true;
            rb.AddForce(new Vector2(0,10000f));
            health--;
        }
    }
}
