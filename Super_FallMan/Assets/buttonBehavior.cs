﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonBehavior : MonoBehaviour
{
    public void onButtonPress() {

        SceneManager.LoadScene("Level1", LoadSceneMode.Single);

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
