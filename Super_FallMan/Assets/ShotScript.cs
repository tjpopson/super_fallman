﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotScript : MonoBehaviour
{
    public GameObject self;
    public float life = 1f;
    public float speed = 1;

    private bool onGround = false;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        life -= Time.deltaTime;
        groundCheck.position = new Vector3(transform.position.x,transform.position.y-(transform.localScale.y/2)+.1f,transform.position.z);
        onGround = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        if (life <= 0 || onGround)
            Destroy(self);
        
    }

    private void FixedUpdate()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z) ;
    }
}
