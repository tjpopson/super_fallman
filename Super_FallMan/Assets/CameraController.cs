﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(-.5f, player.localPosition.y, transform.localPosition.z);
    }
}
